import 'package:flutter/material.dart';
import 'package:flutter_application_1/signup.dart';

void main() {
  runApp(dashboard());
}

class dashboard extends StatelessWidget {
  const dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SHOPPING SPREE',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        accentColor: Colors.purple,
        scaffoldBackgroundColor: Colors.purple[900],
      ),
      home: Scaffold(
        appBar: AppBar(title: const Text('Dashboard')),
        body: const dashBoard(),
      ),
    );
  }
}

class dashBoard extends StatefulWidget {
  const dashBoard({Key? key}) : super(key: key);

  @override
  _dashBoardState createState() => _dashBoardState();
}

class _dashBoardState extends State<dashBoard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(children: <Widget>[
          const Image(
            image: AssetImage('images/summer.png'),
            width: 80,
            height: 80,
          ),
          TextButton(
            child: const Text('Summer collection'),
            onPressed: () {},
          ),
          const Image(
            image: AssetImage('images/winter.jpg'),
            width: 80,
            height: 80,
          ),
          TextButton(
            child: const Text('Winter collection'),
            onPressed: () {},
          ),
          Row(
            mainAxisAlignment: (MainAxisAlignment.center),
            children: <Widget>[
              const Text('Seen enough? Why not'),
              TextButton(
                child: const Text('sign up'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const signupscreen()),
                  );
                },
              ),
            ],
          ),
        ]),
      ),
    );
  }
}
